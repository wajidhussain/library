<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    public function rackName()
    {
         return $this->hasOne('App\Rack','id','rack_id');
    }
    public function belong()
    {
    	return $this->belongsTo('App\Rack');
    }
}
