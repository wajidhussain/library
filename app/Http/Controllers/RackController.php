<?php

namespace App\Http\Controllers;

use App\Rack;
use App\Book;
use Illuminate\Http\Request;

class RackController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $racks=Rack::orderby('id','desc')->get();
        
        return view('rack.list',compact('racks'));
    }
   
    // to get racks list with total number number of books

    public function rackList()
    {
        $racks = \DB::select( \DB::raw("SELECT racks.id, max(racks.rack_name) as rack_name ,COUNT(books.book_title) as totalBooks FROM books RIGHT JOIN racks on racks.id=books.rack_id GROUP by racks.id") );
        
        return view('rack.racklist',compact('racks'));
    }

    public function searchRack()
    {
        return view('rack.search');
    }

    public function searchResult(Request $request)
    {
        $search_term=$request->search_term;
        $results = \DB::table('books')
            ->select('*')
            ->join('racks', 'racks.id', '=', 'books.rack_id')
            ->where('books.book_title','like',"%{$search_term}%")
            ->Orwhere('books.author','like',"%{$search_term}%")
            ->Orwhere('books.published','like',"%{$search_term}%")
            ->get();

        return view('rack.search',compact("results","search_term"));
    }

    public function rackDetail(Request $request)
    {
        $books = Book::where('rack_id',$request->rack_id)->get();

        return $books;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(\Auth::user()->hasRole('Admin'))
            {
                return view('rack.add');
            }
            else
            {
                return view('error');
            }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'rack_name' => 'required',
        ];

        $this->validate($request, $rules);
        $r = Rack::where('rack_name',$request->input('rack_name'))->get();
        if(count($r) == 0)
        {
            $rack = new Rack();
            $rack->rack_name = $request->input('rack_name');
            $rack->save();

            return redirect('rack')->with('status', 'Rack has been added successfully!');
         }
        else
        {
            return redirect()->back()->withInput($request->input())->with("status", "Rack already exists!");
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(\Auth::user()->hasRole('Admin'))
        {
             $rack = Rack::findOrFail($id);
           
             return view('rack.add', compact('rack'));
         }
         else
         {
            return view('error');
         }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $rules = [
            'rack_name' => 'required',
        ];

        $this->validate($request, $rules);

        $rack = Rack::findOrFail($id);
        $rack->rack_name = $request->input('rack_name');
        $rack->save();

        return redirect()->back()->with("status", "Rack has been updated successfully.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(\Auth::user()->hasRole('Admin'))
        {
            $rack = Rack::findOrFail($id);
            $book = Book::where('rack_id',$id)->delete();
            $rack->delete();

            return redirect()->back()->with("status", "Rack has been deleted successfully.");
        }
        else
        {
            return view('error');
        }
    }
}
