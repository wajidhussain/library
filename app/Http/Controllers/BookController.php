<?php

namespace App\Http\Controllers;

use App\Book;
use App\Rack;
use Illuminate\Http\Request;

class BookController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books=Book::with('rackName')->orderby('id','desc')->get();

        return view('book.list',compact('books'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(\Auth::user()->hasRole('Admin'))
            {
               $racks = Rack::get();
               return view('book.add',compact('racks'));
        }
        else{
            return view('error');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'book_title' => 'required',
            'author' => 'required',
            'published' => 'required',
            'rack_id' => 'required',
        ];

        $this->validate($request, $rules);
        // to check already exists or not
        $bok = Book::where('book_title',$request->input('book_title'))->get();
        // to check total 10 books in a rack
        $b = Book::where('rack_id',$request->input('rack_id'))->get();
        
        if(count($bok) == 0)
        {
            if(count($b) <= 10)
            {
                $book = new Book();
                $book->book_title = $request->input('book_title');
                $book->author = $request->input('author');
                $book->published = $request->input('published');
                $book->rack_id = $request->input('rack_id');
                $book->save();

                return redirect('book')->with('status', 'Book has been added successfully!');
            }
            else
            {
                   return redirect()->back()->withInput($request->input())->with("status", "Only 10 books can be added in a rack.");
            }
        }
        else
        {
            return redirect()->back()->withInput($request->input())->with("status", "Book already exists!");
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(\Auth::user()->hasRole('Admin'))
        {
             $book = Book::findOrFail($id);
             $racks = Rack::get();       
             return view('book.add', compact('book','racks'));
         }
         else
         {
            return view('error');
         }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $rules = [
            'book_title' => 'required',
            'author' => 'required',
            'published' => 'required',
            'rack_id' => 'required',
        ];

        $this->validate($request, $rules);
        $book = Book::findOrFail($id);
       
        $book->book_title = $request->input('book_title');
        $book->author = $request->input('author');
        $book->published = $request->input('published');
        $book->rack_id = $request->input('rack_id');
   
        $book->save();
         return redirect()->back()->with("status", "Book has been updated successfully.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(\Auth::user()->hasRole('Admin'))
        {
            $book = Book::findOrFail($id);
            $book->delete();
            return redirect()->back()->with("status", "Book has been deleted successfully.");
        }
        else
        {
            return view('error');
        }
    }
}
