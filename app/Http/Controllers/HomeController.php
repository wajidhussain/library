<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rack;
use App\Book;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(\Auth::user()->hasRole('Admin'))
        {
            $books = Book::get();
            $racks = Rack::get();
            return view('admin.home',compact('books','racks'));
        }
        else
        {
            $books = Book::get();
            $racks = Rack::get();
            return view('home',compact('books','racks'));
        }
       
    }

}
