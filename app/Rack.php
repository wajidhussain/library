<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rack extends Model
{
    public function belong()
    {
    	return $this->belongsTo('App\Book');
    }

    public function totalBooks()
    {
    	return $this->hasMany('App\Book','rack_id','id');
    }
}
