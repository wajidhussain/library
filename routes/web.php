<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('book','BookController');
Route::resource('rack','RackController');

Route::get('racks-list','RackController@rackList');
Route::get('search','RackController@searchRack');
Route::post('search','RackController@searchResult');
Route::post('get-detail','RackController@rackDetail');
