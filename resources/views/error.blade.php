@extends('layouts.app')
@extends('layouts.navigation')
@section('content')
<div class="container">
   <div class="row">
        <div class="alert alert-danger col-sm-8">
            <h2>Sorry you dont have access to this page.</h2>
        </div>
    </div>
</div>
@endsection
