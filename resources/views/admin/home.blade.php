@extends('layouts.app')
@extends('layouts.navigation')
@section('content')
     <div class="row">
        <div class="col-lg-2">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Total Racks</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins">{{!empty($racks)?count($racks):0}}</h1>
                  
                </div>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Total Books</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins">{{!empty($books)?count($books):0}}</h1>
                    
                </div>
            </div>
        </div>
       
    </div>
    
@endsection
