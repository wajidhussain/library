 @section('navigation')
 <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav metismenu" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element"> <span>
                                <img alt="image" class="img-circle" src="{{asset('img/profile_small.jpg')}}" />
                                 </span>
                        </div>
                        <div class="logo-element">
                           Admin
                        </div>
                    </li>
                @if(\Auth::user()->hasRole('Admin'))
                    <li class="active">
                        <a href="{{url('home')}}"><i class="fa fa-dashboard"></i> <span class="nav-label">Admin Dashboard</span></a>
                    </li>
                    <li>
                        <a href="javascript:;"><i class="fa fa-users"></i> 
                            <span class="nav-label">Racks</span> 
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level">
                            <li><a href="{{url('rack')}}">Racks List</a></li>
                            <li><a href="{{url('rack/create')}}">Add Rack</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;"><i class="fa fa-users"></i> 
                            <span class="nav-label">Books</span> 
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level">
                            <li><a href="{{url('book')}}">Books List</a></li>
                            <li><a href="{{url('book/create')}}">Add Book</a></li>
                        </ul>
                    </li>
                    
                @elseif(\Auth::user()->hasRole('User'))
                    <li class="active">
                        <a href="{{url('home')}}"><i class="fa fa-dashboard"></i> <span class="nav-label">
                        Client Dashboard</span></a>
                    </li>
                    <li>
                        <a href="javascript:;"><i class="fa fa-users"></i> 
                            <span class="nav-label">Racks</span> 
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level">
                            <li><a href="{{url('racks-list')}}">Racks List</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;"><i class="fa fa-users"></i> 
                            <span class="nav-label">Search</span> 
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level">
                            <li><a href="{{url('search')}}">Search Book</a></li>
                        </ul>
                    </li>
                @endif
                
                </ul>
            </div>
        </nav>
@endsection




