@extends('layouts.app')
@extends('layouts.navigation')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Books List</h5>
                <div class="ibox-tools">
                    
                </div>
            </div>
            <div class="ibox-content">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover list-dataTable" >
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Book Title</th>
                            <th>Author</th>
                            <th>Published Date</th>
                            <th>Rack</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>

                        @if(isset($books))
                            @foreach($books as $k=>$book)
                            <tr class="gradeX">
                                <td>{{$k+1}}</td>
                                <td>{{$book->book_title}}</td>
                                <td>{{$book->author}}</td>
                                <td>{{$book->published}}</td>
                                <td>{{$book->rackName['rack_name']}}</td>                                
                                <td>
                                
                                    <a title="Edit" class="btn btn-primary" href="{{Request::url()}}/{{$book->id}}/edit">Edit</a>
                                    <a title="Delete" class="btn btn-danger" onclick="deleteItem('{{Request::url()}}/{{$book->id}}');">Delete</a>
                                    
                                <form id="deleteForm" method="POST" action="">
                                    {!! method_field('DELETE') !!}
                                    {!! csrf_field() !!}
                                </form>
                            </td>
                               
                            </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    function deleteItem(url) {
        if ($('#deleteForm').length > 0) {
            if (confirm('Are you sure you want to delete this?')) {
                $('#deleteForm').attr('action', url);
                $('#deleteForm').submit();
            }
        }
    }
</script>
@endsection