@extends('layouts.app')
@extends('layouts.navigation')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>{{empty($book)? 'Add Book':'Update Book'}}</h5>
            </div>
           
            <div class="ibox-content">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                <form method="POST" action="{{ url('book'.(empty($book) ? '': '/' . $book->id)) }}"
                 enctype="multipart/form-data">
                  {{ csrf_field() }}
                    <div class="form-group row {{ $errors->has('book_title') ? ' has-error' : '' }}">
                      <label class="col-sm-2 col-form-label">Book Title <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                      
                            <input type="text" name="book_title" class="form-control" value="{{old('book_title', @$book->book_title)}}">
                          
                            @if ($errors->has('book_title'))
                                <span class="error"><strong>{{ $errors->first('book_title') }}</strong></span>
                            @endif
                        </div>
                    </div> 

                    <div class="form-group row {{ $errors->has('author') ? ' has-error' : '' }}">
                      <label class="col-sm-2 col-form-label">Book Author <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                      
                            <input type="text" name="author" class="form-control" value="{{old('author', @$book->author)}}">
                          
                            @if ($errors->has('author'))
                                <span class="error"><strong>{{ $errors->first('author') }}</strong></span>
                            @endif
                        </div>
                    </div> 

                    <div class="form-group row {{ $errors->has('published') ? ' has-error' : '' }}">
                      <label class="col-sm-2 col-form-label">Published Date <span class="text-danger">*</span></label>
                        <div class="col-sm-4">
                      
                            <input type="date" name="published" class="form-control" value="{{old('published', @$book->published)}}">
                          
                            @if ($errors->has('published'))
                                <span class="error"><strong>{{ $errors->first('published') }}</strong></span>
                            @endif
                        </div>
                    </div> 

                    <div class="form-group row {{ $errors->has('rack_id') ? ' has-error' : '' }}">
                      <label class="col-sm-2 col-form-label"> Choose Rack <span class="text-danger">*</span></label>
                        <div class="col-sm-4">
                            <select name="rack_id" class="form-control">
                                <option value="">Select Rack</option>
                                @if(isset($racks))
                                    @foreach($racks as $rack)
                                       @php 
                                            
                                                $s=(@$book->rack_id==$rack->id) ? 'selected':''; 
                                         
                                       @endphp
                                        <option {{$s}} value="{{$rack->id}}">{{$rack->rack_name}}</option>
                                    @endforeach
                                @endif
                            </select>
                          
                            @if ($errors->has('rack_id'))
                                <span class="error"><strong>{{ $errors->first('rack_id') }}</strong></span>
                            @endif
                        </div>
                    </div> 
                                            
                    <div class="hr-line-dashed"></div>
                    @if(!empty($book)) {!! method_field('PUT') !!} @endif
                
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-primary" type="submit"> @if(empty($book)) Save @else Update
                                @endif</button>
                        </div>
                    </div>
               </form>
            </div>
        </div>
    </div>
@endsection