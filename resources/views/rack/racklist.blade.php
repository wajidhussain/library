@extends('layouts.app')
@extends('layouts.navigation')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Racks List</h5>
                <div class="ibox-tools">
                    
                </div>
            </div>
            <div class="ibox-content">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover list-dataTable" >
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Rack Name</th>
                            <th>Total Books</th>
                        </tr>
                        </thead>
                        <tbody>

                        @if(isset($racks))
                            @foreach($racks as $k=>$rack)
                            <tr class="gradeX">
                                <td>{{$k+1}}</td>
                                <td><a href="javascript:void(0)" class="detail" data-value="{{$rack->id}}" data-toggle="modal" >{{$rack->rack_name}}</a></td>                              
                                <td>{{$rack->totalBooks}}</td>
                            
                            </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Rack Detail</h4>
        </div>
        <div class="modal-body">
          <table class="table table-striped table-bordered table-hover" >
              <th>Book Title</th>
              <th>Author</th>
              <th>Published</th>
              <tbody id="menu">
                  
              </tbody>
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
@endsection
@section('scripts')
<script>
    function deleteItem(url) {
        if ($('#deleteForm').length > 0) {
            if (confirm('Are you sure you want to delete this?')) {
                $('#deleteForm').attr('action', url);
                $('#deleteForm').submit();
            }
        }
    }

    $('.detail').click(function(){

        rack_id=$(this).attr('data-value');

         $.ajax({
             type:"POST",
             url:"/get-detail",
             dataType:"json",
             headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
             data:{"rack_id":rack_id},
             success: function(d)
             {
                $('#menu').html('');
                if(d != '')
                {
                   $.each( d, function( key, value ) {
                       $('#menu').append('<tr><td>'+value.book_title+'</td><td>'+value.author+'</td><td>'+value.published+'</td></tr>');
                    });
                   $('#myModal').modal('show');
                  }
                else
                {
                    $('#menu').append('<tr><td colspan="3">No record found</td></tr>');
                    $('#myModal').modal('show');
                }

             },
             error: function(e)
             {
                $('#myModal').modal('hide');
                 alert('something went wrong');

             }
        });
    });
</script>
@endsection