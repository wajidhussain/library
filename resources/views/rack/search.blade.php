@extends('layouts.app')
@extends('layouts.navigation')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Search Book</h5>
            </div>
            <div class="ibox-content">
                <form method="POST" action="{{ url('search') }}"
                 enctype="multipart/form-data">
                  {{ csrf_field() }}
                    <div class="form-group row {{ $errors->has('search_term') ? ' has-error' : '' }}">
                      <label class="col-sm-2 col-form-label">Search <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                      
                            <input type="text" name="search_term" class="form-control" value="{{old('search_term',@$search_term)}}">
                          
                        </div>
                    </div> 
                    <div class="hr-line-dashed"></div>
                
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-primary" type="submit"> Search
                            </button>
                        </div>
                    </div>
               </form>
            </div>
        </div>
    </div>
</div>
@if(isset($results))
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Search Results</h5>
                <div class="ibox-tools">
                    
                </div>
            </div>
            <div class="ibox-content">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" >
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Book Title</th>
                            <th>Author</th>
                            <th>Published Date</th>
                            <th>Rack Name</th>
                        </tr>
                        </thead>
                        <tbody>
                            @if(count($results) > 0)
                            @foreach($results as $k=>$result)
                                <tr>
                                    <td>{{$k+1}}</td>
                                    <td>{{$result->book_title}}</td>
                                    <td>{{$result->author}}</td>
                                    <td>{{$result->published}}</td>
                                    <td>{{$result->rack_name}}</td>
                                </tr>
                            @endforeach
                            @else
                                <tr>
                                    <td colspan="5">No Record Found!</td>
                                </tr>
                            @endif
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
@endsection