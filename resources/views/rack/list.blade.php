@extends('layouts.app')
@extends('layouts.navigation')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Racks List</h5>
                <div class="ibox-tools">
                    
                </div>
            </div>
            <div class="ibox-content">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover list-dataTable" >
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Rack Name</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>

                        @if(isset($racks))
                            @foreach($racks as $k=>$rack)
                            <tr class="gradeX">
                                <td>{{$k+1}}</td>
                                <td>{{$rack->rack_name}}</td>                              
                                <td>
                                
                                    <a title="Edit" class="btn btn-primary" href="{{Request::url()}}/{{$rack->id}}/edit">Edit</a>
                                    <a title="Delete" class="btn btn-danger" onclick="deleteItem('{{Request::url()}}/{{$rack->id}}');">Delete</a>
                                    
                                <form id="deleteForm" method="POST" action="">
                                    {!! method_field('DELETE') !!}
                                    {!! csrf_field() !!}
                                </form>
                                </td>
                            
                               
                            </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    function deleteItem(url) {
        if ($('#deleteForm').length > 0) {
            if (confirm('Are you sure?. All books will also be deleted of this rack.')) {
                $('#deleteForm').attr('action', url);
                $('#deleteForm').submit();
            }
        }
    }
</script>
@endsection