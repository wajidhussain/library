@extends('layouts.app')
@extends('layouts.navigation')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>{{empty($rack)? 'Add Rack':'Update Rack'}}</h5>
            </div>
           
            <div class="ibox-content">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                <form method="POST" action="{{ url('rack'.(empty($rack) ? '': '/' . $rack->id)) }}"
                 enctype="multipart/form-data">
                  {{ csrf_field() }}
                    <div class="form-group row {{ $errors->has('rack_name') ? ' has-error' : '' }}">
                      <label class="col-sm-2 col-form-label">Rack Name <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                      
                            <input type="text" name="rack_name" class="form-control" value="{{old('rack_name', @$rack->rack_name)}}">
                          
                            @if ($errors->has('rack_name'))
                                <span class="error"><strong>{{ $errors->first('rack_name') }}</strong></span>
                            @endif
                        </div>
                    </div> 

                                            
                    <div class="hr-line-dashed"></div>
                    @if(!empty($rack)) {!! method_field('PUT') !!} @endif
                
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-primary" type="submit"> @if(empty($rack)) Save @else Update
                                @endif</button>
                        </div>
                    </div>
               </form>
            </div>
        </div>
    </div>
@endsection