function showNotification(type,message) {
	setTimeout(function() {
	    toastr.options = {
	        closeButton: true,
	        progressBar: true,
	        showMethod: 'slideDown',
	        timeOut: 3000
	    };
	    if(type == 'success'){
	    	toastr.success(message);
	    }else{
			toastr.error("error");
	    }

	}, 1300);
 } 
$(document).ready(function() {
	$(".error").hide();
    $('#categoryAddForm').ajaxForm(function(result) { 
        obj = JSON.parse(result);
        if(obj.success){
			showNotification('success', obj.message);
			$('#categoryAddForm')[0].reset();
        }else{
        	$("#input-name").html(obj.errors.name).show();
        }
    }); 

    //Update Category
    $('#categoryUpdateForm').ajaxForm(function(result) { 
        obj = JSON.parse(result);
        if(obj.success){
			showNotification('success', obj.message);
			//$('#categoryAddForm')[0].reset();
        }else{
        	showNotification('error', obj.message);
        }
    });

    //Delete Category

$('.deletecat').on('click', function(e) {
    var inputData = $('#formCat').serialize();

    var dataId = $(this).attr('data-id');

    $.ajax({
        url: '/categories/delete/'+dataId,
        type: 'GET',
        data: inputData,
        success: function( result ) {
            obj = JSON.parse(result);
            if ( obj.success == true ) {
                showNotification('success', obj.message);
                setInterval(function() {
                    window.location.reload();
                }, 3000);
            }
            else
            {
                showNotification('error', obj.message);
            }
        },
        error: function( data ) {
            if ( data.status === 422 ) {
                toastr.error('Cannot delete the category');
            }
        }
    });

    return false;
});


           //Sub Category

       $('#subcategoryAddForm').ajaxForm(function(result) { 
        obj = JSON.parse(result);
        if(obj.success){
			showNotification('success', obj.message);
			$('#subcategoryAddForm')[0].reset();
        }else{
        	$("#input-name").html(obj.errors.name).show();
        	$("#input-categoryid").html(obj.errors.category_id).show();
        }
    }); 

       // SubCategory Update

    $('#subcategoryUpdateForm').ajaxForm(function(result) { 
        obj = JSON.parse(result);
        if(obj.success){
			showNotification('success', obj.message);
			//$('#categoryAddForm')[0].reset();
        }else{
        	showNotification('error', obj.message);
        }
    });

      //SubCategory Deleted

$('.deleteSubcat').on('click', function(e) {
    var inputData = $('#formSubCat').serialize();

    var dataId = $(this).attr('data-id');

    $.ajax({
        url: '/subcategories/delete/'+dataId,
        type: 'GET',
        data: inputData,
        success: function( result ) {
            obj = JSON.parse(result);
            if ( obj.success == true ) {
                showNotification('success', obj.message);
                setInterval(function() {
                    window.location.reload();
                }, 3000);
            }
            else
            {
                showNotification('error', obj.message);
            }
        },
        error: function( data ) {
            if ( data.status === 422 ) {
                toastr.error('Cannot delete the category');
            }
        }
    });

    return false;
});



    //User Role 

     $('#userroleAddForm').ajaxForm(function(result) { 
        obj = JSON.parse(result);
        if(obj.success){
			showNotification('success', obj.message);
			$('#userroleAddForm')[0].reset();
        }else{
        	$("#input-name").html(obj.errors.name).show();
        }
    }); 

     // Update UserRole

    $('#userroleUpdateForm').ajaxForm(function(result) { 
        obj = JSON.parse(result);
        if(obj.success){
			showNotification('success', obj.message);
			//$('#categoryAddForm')[0].reset();
        }else{
        	showNotification('error', obj.message);
        }
    });

       //User-Role Deleted

$('.deleteRole').on('click', function(e) {
    var inputData = $('#formUserRole').serialize();

    var dataId = $(this).attr('data-id');

    $.ajax({
        url: '/user-role/delete/'+dataId,
        type: 'GET',
        data: inputData,
        success: function( result ) {
            obj = JSON.parse(result);
            if ( obj.success == true ) {
                showNotification('success', obj.message);
                setInterval(function() {
                    window.location.reload();
                }, 3000);
            }
            else
            {
                showNotification('error', obj.message);
            }
        },
        error: function( data ) {
            if ( data.status === 422 ) {
                toastr.error('Cannot delete the category');
            }
        }
    });

    return false;
});


    //User Info 

     $('#userinfoAddForm').ajaxForm(function(result) { 
        obj = JSON.parse(result);
        if(obj.success){
			showNotification('success', obj.message);
			$('#userinfoAddForm')[0].reset();
        }else{
        	$("#input-name").html(obj.errors.name).show();
        	$("#input-email").html(obj.errors.name).show();
        	$("#input-password").html(obj.errors.name).show();
        	$("#input-address").html(obj.errors.name).show();
        	$("#input-phone").html(obj.errors.name).show();
        	$("#input-userrole").html(obj.errors.name).show();
        }
    }); 
    $('#userinfoUpdateForm').ajaxForm(function(result) { 
        obj = JSON.parse(result);
        if(obj.success){
			showNotification('success', obj.message);
			//$('#categoryAddForm')[0].reset();
        }else{
        	showNotification('error', obj.message);
        }
    });

    //User-Info Deleted

$('.deleteInfo').on('click', function(e) {
    var inputData = $('#formUserInfo').serialize();

    var dataId = $(this).attr('data-id');

    $.ajax({
        url: '/user-info/delete/'+dataId,
        type: 'GET',
        data: inputData,
        success: function( result ) {
            obj = JSON.parse(result);
            if ( obj.success == true ) {
                showNotification('success', obj.message);
                setInterval(function() {
                    window.location.reload();
                }, 3000);
            }
            else
            {
                showNotification('error', obj.message);
            }
        },
        error: function( data ) {
            if ( data.status === 422 ) {
                toastr.error('Cannot delete the category');
            }
        }
    });

    return false;
});



// Get SubCategory against Category dropdown changes

      $('select[name="category_id"]').on('change', function() {
            var stateID = $(this).val();
            if(stateID) {
                $.ajax({
                    url: '/product/subcategories/'+stateID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {
                            console.log(data);
                        
                        $('select[name="subcategory_id"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="subcategory_id"]').append('<option value="'+ value.id +'">'+ value.name +'</option>');
                        });
                    }
                });
            }else{
                $('select[name="subcategory_id"]').empty();
            }
        });

// Product Add

    $('#productAddForm').ajaxForm(function(result) { 
        obj = JSON.parse(result);
        if(obj.success){
            showNotification('success', obj.message);
            $('#productAddForm')[0].reset();
        }else{
            $("#input-name").html(obj.errors.name).show();
            $("#input-categoryid").html(obj.errors.categoryid).show();
            $("#input-subcategory_id").html(obj.errors.subcategory_id).show();
            $("#input-quantity").html(obj.errors.quantity).show();
            $("#input-serialno").html(obj.errors.serialno).show();
            $("#input-product_image").html(obj.errors.product_image).show();
        }
    }); 

    //Product Edit

    $('#productUpdateForm').ajaxForm(function(result) { 
        obj = JSON.parse(result);
        if(obj.success){
            showNotification('success', obj.message);
        }else{
            showNotification('error', obj.message);
        }
    });


    //Product Delete

    $('.deleteProduct').on('click', function(e) {
    var inputData = $('#formProduct').serialize();

    var dataId = $(this).attr('data-id');

    $.ajax({
        url: '/product/delete/'+dataId,
        type: 'GET',
        data: inputData,
        success: function( result ) {
            obj = JSON.parse(result);
            if ( obj.success == true ) {
                showNotification('success', obj.message);
                setInterval(function() {
                    window.location.reload();
                }, 3000);
            }
            else
            {
                showNotification('error', obj.message);
            }
        },
        error: function( data ) {
            if ( data.status === 422 ) {
                toastr.error('Cannot delete the category');
            }
        }
    });

    return false;
});






// Repairing Parts Add

    $('#RepairingPartAddForm').ajaxForm(function(result) { 
        obj = JSON.parse(result);
        if(obj.success){
            showNotification('success', obj.message);
            $('#RepairingPartAddForm')[0].reset();
        }else{
            $("#input-name").html(obj.errors.name).show();
            $("#input-parts_image").html(obj.errors.parts_image).show();
        }
    }); 

    //Repairing Parts Edit

    $('#repairingpartsUpdateForm').ajaxForm(function(result) { 
        obj = JSON.parse(result);
        if(obj.success){
            showNotification('success', obj.message);
        }else{
            showNotification('error', obj.message);
        }
    });


    //Product Delete

    $('.deleteParts').on('click', function(e) {
    var inputData = $('#formParts').serialize();

    var dataId = $(this).attr('data-id');

    $.ajax({
        url: '/repairing-part/delete/'+dataId,
        type: 'GET',
        data: inputData,
        success: function( result ) {
            obj = JSON.parse(result);
            if ( obj.success == true ) {
                showNotification('success', obj.message);
                setInterval(function() {
                    window.location.reload();
                }, 3000);
            }
            else
            {
                showNotification('error', obj.message);
            }
        },
        error: function( data ) {
            if ( data.status === 422 ) {
                toastr.error('Cannot delete the category');
            }
        }
    });

    return false;
});


//delete slider

$('.deleteSlider').on('click', function(e) {
    
    var dataId = $(this).attr('data-id');

    $.ajax({
        url: '/slider/delete/'+dataId,
        type: 'GET',
        //data: inputData,
        success: function( result ) {
            obj = JSON.parse(result);
            if ( obj.success == true ) {
                showNotification('success', obj.message);
                setInterval(function() {
                    window.location.reload();
                }, 3000);
            }
            else
            {
                showNotification('error', obj.message);
            }
        },
        error: function( data ) {
            if ( data.status === 422 ) {
                toastr.error('Cannot delete the category');
            }
        }
    });

    return false;
});

//activate and deactivate slider

$('.activateSlider').on('click', function(e) {
    
    var dataId = $(this).attr('data-id');
    var status = $(this).attr('data-status');


    $.ajax({
        url: '/slider/activate/'+dataId+'/'+status,
        type: 'GET',
        //data: inputData,
        success: function( result ) {
            obj = JSON.parse(result);
            if ( obj.success == true ) {
                showNotification('success', obj.message);
                setInterval(function() {
                    window.location.reload();
                }, 3000);
            }
            else
            {
                showNotification('error', obj.message);
            }
        },
        error: function( data ) {
            if ( data.status === 422 ) {
                toastr.error('Cannot delete the category');
            }
        }
    });

    return false;
});

$('.editSlider').click(function(e){

    $('#editbox').toggle();
});



});
