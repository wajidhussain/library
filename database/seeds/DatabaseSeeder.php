<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('123456'),
        ]);
        DB::table('roles')->insert([
            'name' => 'Admin',
            
    	]);
    	DB::table('roles')->insert([
           'name' => 'User',
            
    	]);
	     DB::table('role_user')->insert([
        	'user_id' => 1,
        	'role_id' => 1,
        
        ]);
         // $this->call(UserTableSeeder::class);

         // $this->command->info('User table seeded!');
    }
}

